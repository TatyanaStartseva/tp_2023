#include "iomanip.hpp"
#include <iostream>
#include <cmath>

int main()
{
  std::ios_base::fmtflags flags = std::cout.flags( ); // Save old flags

  std::cout << "pi (std::scientific) = " << std::scientific << M_PI << '\n'; // Scientific mode

 std::cout << "pi (floatnormal) = " << floatnormal << M_PI << '\n'; // custom

 std::cout.flags(flags);

  Price price{ 15050 };

  std::cout << "price (default) = " << price << '\n';
  std::cout << "price (price.price) = " << price.price << '\n';

  // проверим, что состояние потока такое же, как до манипулятора, scope guard работает
  // std::cout << 14.151515 << '\n';

  // оператор вывода с манипуляторами
  std::cout << "price (exact) = " << exact << price << '\n';
  std::cout << "price (fractional) = " << fractional << price << '\n';


 return 0;
}